// Fill the DB with example data on startup

import { Meteor } from 'meteor/meteor';
import { Links } from '../../api/links/links.js';
import { Migrations } from '../../api/migrations/migrations';
import { Roles } from 'meteor/alanning:roles';

Meteor.startup(() => {
	if (!Migrations.findOne({ slug: 'users' })) {

		Roles.createRole("admin");

		var users = [
			{ name: "Admin Dani", email: "dani@admin.com", pass: "admin123", roles: ['admin'] },
			{ name: "Admin Christian", email: "cristian@vifuy.com", pass: "vifuychobits", roles: ['admin'] },
		];

		for (user of users) {

			id = Accounts.createUser({
				email: user.email,
				password: user.pass,
				profile: {
					name: user.name,
					status: true,
				}
			});

			if (user.roles.length > 0) {
				// Need _id of existing user record so this call must come
				// after `Accounts.createUser` or `Accounts.onCreate`
				Roles.addUsersToRoles(id, user.roles);
			}

		}

		Migrations.insert({ slug: 'users' });
		console.log('Running migrations for users...');
	}
});
