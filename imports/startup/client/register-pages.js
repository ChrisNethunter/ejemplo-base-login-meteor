//Importing Layouts

import '../../ui/layouts/body/body';

    //Importing Pages

    import '../../ui/pages/home/home';
    import '../../ui/pages/Login/Login';
    import '../../ui/pages/Admin/Admin';

    import '../../ui/pages/not-found/not-found.js';