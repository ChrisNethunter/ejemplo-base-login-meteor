import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import register-pages

import './register-pages';

// Set up all routes in the app
FlowRouter.route('/', {
	name: 'App.home',
	action() {
		BlazeLayout.render('App_body', { main: 'App_home' });
	},
});


/*FlowRouter.route('/login', {
	name: 'App.login',
	action() {
		BlazeLayout.render('App_body', { main: 'App_login' });
	},
});*/


FlowRouter.route('/admin', {
	name: 'App.admin',
	action() {
		BlazeLayout.render('App_body', { main: 'App_admin' });
	},
});


FlowRouter.notFound = {
	action() {
		BlazeLayout.render('App_body', { main: 'App_notFound' });
	},
};
