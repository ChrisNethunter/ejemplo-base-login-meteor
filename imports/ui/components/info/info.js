import { Links } from '/imports/api/links/links.js';
import { Meteor } from 'meteor/meteor';
import './info.html';


var dataLinks = new ReactiveVar([]);
var dataUsers = new ReactiveVar([]);
var idToEditLink;


Template.info.onCreated( () => {

	realTimeDataLinks();
	getDataUsers();

	//Meteor.subscribe('links.all');

});

Template.info.onRendered(() => {


});


Template.info.helpers({
	
	links() {
		return dataLinks.get();
	},

	nameUser ( _idUser  ) {

		let getPositionDataUser = dataUsers.get().findIndex(( item, key) => {
			return item._id == _idUser
		});

		return  dataUsers.get()[ getPositionDataUser ].profile.name;  

	},

	removeUserShow ( _idUser ) {

		if( _idUser == Meteor.userId() ){

			return true 

		}

	}

/* 	lista_amigos() {
		return [

			{
				name : 'amigo 1',
				age : 24
			},

			{
				name : 'amigo 2',
				age : 30
			},

			{
				name : 'amigo 3',
				age : 15
			},

			{
				name : 'amigo 4',
				age : 12
			}

		]
	}, */
});

Template.info.events({
	
	'submit .info-link-add'(event) {
		event.preventDefault();

		const target = event.target;
		const title = target.title;
		const url = target.url;

		Meteor.call('api/links/insert', title.value, url.value, (error) => {
			if (error) {

				Bert.alert( error.error , 'danger');
			
			} else {
				
				Bert.alert( 'god job insert' , 'success');
				title.value = '';
				url.value = '';
				
				realTimeDataLinks();

			}
		});



	},

	'click .remove'(event) {
		event.preventDefault();

		let target_id_link_to_remove = event.currentTarget.id;

		console.log( target_id_link_to_remove )


		Meteor.call('api/links/remove' , target_id_link_to_remove , ( error , response ) => {
			if( error ) {
	
				Bert.alert( error.error , 'danger');
	
			}else{

				console.log( response )

				realTimeDataLinks();

				Bert.alert( 'god job link removed' , 'success');

			}
		})

	},

	'click .edit'(event) {
		event.preventDefault();

		let target_id_link_to_edit = event.currentTarget.id;

		idToEditLink = target_id_link_to_edit;
		
		document.getElementById('create').style.display = 'none';
		document.getElementById('edit').style.display = 'block';


		Meteor.call('api/links/getLink' ,  target_id_link_to_edit  ,( error , response_data_link ) => {
			if( !error ) {
				
				document.getElementById('title-edit').value = response_data_link.title;
				document.getElementById('url-edit').value = response_data_link.url;
				
			}
		})

	},

	'click #cancel-edit'(event) {
		event.preventDefault();

		document.getElementById('create').style.display = 'block';
		document.getElementById('edit').style.display = 'none';

	},

	'submit #edit'(event) {
		event.preventDefault();

		const target_id_link_to_edit = event.currentTarget.id;
		const target = event.target;
		const title = target.titleEdit;
		const url = target.urlEdit;

		Meteor.call('api/links/edit', idToEditLink , title.value, url.value, (error) => {
			if (error) {

				Bert.alert( error.error , 'danger');
			
			} else {
				
				Bert.alert( 'god job edit' , 'success');
				title.value = '';
				url.value = '';
				
				realTimeDataLinks();
				document.getElementById('edit').click();


			}
		});



	},
});

const realTimeDataLinks = () => {

	Meteor.call('api/links/seachall' , ( error , response_data_links ) => {
		if( !error ) {

			dataLinks.set( response_data_links )

		}
	})

}

const getDataUsers = () => {

	Meteor.call('api/getAllUsers' , ( error , response_all_users ) => {
		if( !error ) {
			dataUsers.set( response_all_users )
		}
	})

}