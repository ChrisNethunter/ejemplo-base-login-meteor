import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './Login.html';

Template.App_login.onCreated( () => {

});

Template.App_login.onRendered(() => {

    Meteor.setTimeout( ( ) => { 
        //alert("Hello"); 
    }, 1000);

});

Template.App_login.helpers({
    
});

Template.App_login.events({


    'click #show-signin'(event, instance) {
        event.preventDefault();

        document.getElementById('login').style.display = 'none';
        document.getElementById('signin').style.display = 'block';

    },

    'click #show-login'(event, instance) {
        event.preventDefault();

        document.getElementById('signin').style.display = 'none';
        document.getElementById('login').style.display = 'block';

    },


    'submit #signin'(event, instance) {
        event.preventDefault();

        let ready = false;

        let object_user = {
            email : document.getElementById('email-register').value,
            name : document.getElementById('name-register').value,
            password: document.getElementById('password-register').value,
            passwordConfirm: document.getElementById('password-confirm').value
        }  
        

    
        if( object_user.email != '' ){
            ready = true;
        }else{
        
            Bert.alert( 'email is empty', 'danger')
        }


        if( object_user.password ==  object_user.passwordConfirm ){
            ready = true;
        }else{
      
            Bert.alert( 'password and password confirm are no the same', 'danger')
        }


        if( ready ){

            Meteor.call('create_user', object_user, ( error, response ) => {
                if (error) {
        
                    Bert.alert( String( error ) , 'danger')
        
                } else {
    
                    Bert.alert('User created', 'success');

                    document.getElementById('show-login').click();

                }
            });
        }   

    },

    'submit #login'(event, instance) {
        event.preventDefault();

        let ready = false;

        let email = document.getElementById('email').value,
        password = document.getElementById('password').value;

        if(email != '' ){
            ready = true;
        }

        if( password != '' ){
            ready = true
        }


        Meteor.loginWithPassword(email, password, function (err, res) {
            if (err) {
              
                if (err.message === 'User not found [403]') {


                    Bert.alert( 'User not found [403]' , 'danger');

                } else {

                    Bert.alert( 'password wrong' , 'danger');

                }
                
            } else {
               
                FlowRouter.go('App.home');

            }
        });

    },
    
});

Template.App_login.onCreated( () => {
    
}); 


