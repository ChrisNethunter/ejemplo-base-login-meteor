// Methods related to links

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Links } from './links.js';

Meteor.methods({
	
	'api/links/insert'(title, url) {
		
		if (! Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

		check(url, String);
		check(title, String);

		let link = {
			title : title,
			url : url,
			createdAt : new Date(),
			userId : Meteor.userId()
		}

		return Links.insert( link );

		
	},


	'api/links/seachall'( ) {
		
		return Links.find().fetch();

	},

	'api/links/remove'( idLink ) {
		
		if (! Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
		}
		
		let action_to_remove = Links.remove({ _id : idLink , userId : Meteor.userId() });

		if( action_to_remove == 0 ){

			throw new Meteor.Error("not-authorized");

		}else{

			return action_to_remove

		}

	},

	'api/links/getLink'(  idLink ) {
	
		if (! Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
		}
		
		return Links.findOne({ _id : idLink , userId : Meteor.userId()  });

	},

	'api/links/edit'( idLink , title , url ) {

		console.log( { idLink  } )
		console.log( { title  } )
		console.log( { url  } )

		if (! Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
		}
		
		return Links.update({ _id : idLink , userId : Meteor.userId()  }  , { $set : { title : title , url : url , modifyAt : new Date() } } );


	},

});
