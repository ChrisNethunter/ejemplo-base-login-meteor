// Definition of the links collection

import { Mongo } from 'meteor/mongo';

export const Links = new Mongo.Collection('links');

/* 
// Define the schema
LinksSchema = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200,
        optional: false
    },
    url: {
        type: String,
        label: "Url",
        optional: false,
        max: 200,
    },
    createdAt: {
        type: Date,
        label: "createdAt",
    },
    userId: {
        type: String,
        label: "User",
        optional: false
    }
}); */

