import { Meteor } from 'meteor/meteor';



Meteor.methods({

    'create_user'( user ) {
        
        let profile = {
            name: user.name,
            status: true
        }

        id = Accounts.createUser({
            email: user.email,
            password: user.password,
            profile: profile
        });

        Roles.addUsersToRoles( id , ['standard'] );

        return id;


    },


    'api/getAllUsers'(  ) {
        return Meteor.users.find().fetch();
    },
});
