// Client entry point, imports all client code

import '/imports/startup/client';
import '/imports/startup/both';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';